# react-lib-boilerplate

## 介绍

构建 `react npm package` 的脚手架模板，基于 `vite`。

## 使用

- 使用项目模板（推荐）

  ```bash
  git clone https://gitee.com/leopen/react-lib-boilerplate.git
  cd react-lib-boilerplate
  npm install

  # run examples
  npm run dev

  # build lib
  npm run build

  # run test
  npm run test
  ```

- 手动逐步安装（后续更新可能导致此安装过程缺少部分组件）

  ```bash
  # Init by vite react-ts template
  npm init vite@latest react-lib-boilerplate -- --template react-ts
  cd react-lib-boilerplate
  npm install

  # Add test framework

  ## step 1: install jest for testing and a jest transformer: ts-jest
  npm install --save-dev jest @types/jest ts-jest

  ## step 2:install some packages for testing react
  npm install --save-dev @testing-library/jest-dom @testing-library/react

  ## step 3:install a package for mocking css
  npm install --save-dev identity-obj-proxy

  ## step 4:config ts, ts-jest, see jest.config.js for detail
  npx ts-jest config:init

  ## step 5:add test command, see package.json for detail

  # Add eslint and prettier for code style checking and formatting

  ## step 1: install eslint
  npm install --save-dev eslint

  ## step 2: init eslint
  npx eslint --init

  ## step 3: support typescript (can be skipped if you selected typescript in step 2)
  npm install --save-dev @typescript-eslint/parser @typescript-eslint/eslint-plugin

  ## step 4: support react (can be skipped if you selected react in step 2)
  npm install --save-dev eslint-plugin-react

  ## step 5: config eslint, include .eslintrc.json and .eslintignore, see files for detail

  ## step 6: install prettier
  npm install --save-dev prettier eslint-config-prettier

  ## step 7: config prettier, include .prettierrc and .prettierignore, see files for detail

  ## step 8: add prettier to eslint extends, see .eslintrc.json for detail

  # Config for vite build lib, include vite.config.ts and package.json. see https://cn.vitejs.dev/guide/build.html#library-mode

  # More, like install husky, do anything you like
  ```

## 说明

暂无

## FAQ

- Q: 为什么 `React 17` 编译后依然使用 `React.createElement()` ？
- A: 因为 `vite` 采用 `Esbuild` 用来编译 `jsx tsx` 文件，当前还没有特别好的方式来使用，所以使用了 `React 16` 的编译结果。但是可以自动注入 `React` 的引用，参考以下资料可以帮助您更好理解这个问题：
  - [vite jsx](https://cn.vitejs.dev/guide/features.html#jsx)
  - [vite-issue: Support jsx automatic runtime](https://github.com/vitejs/vite/issues/712)
  - [esbuild-issue: Support jsx automatic runtime](https://github.com/evanw/esbuild/issues/334)
  - [vite-react-jsx: is it production ready?](https://github.com/alloc/vite-react-jsx/issues/4)
