# react-lib-boilerplate

## Introduction

Boilerplate for `react npm package`, based on `vite`。

## How to use

```bash
git clone https://gitee.com/leopen/react-lib-boilerplate.git
cd react-lib-boilerplate
npm install

# run examples
npm run dev

# build lib
npm run build

# run test
npm run test
```

## Detail

To be done
