import React from 'react'
import { render } from '@testing-library/react'
import { ReactLibBoilerplate } from '../'

it('ReactLibBoilerplate should render a div with content 2', () => {
  const { queryByText } = render(<ReactLibBoilerplate value="2" />)
  expect(queryByText('2')).toBeTruthy()
})
