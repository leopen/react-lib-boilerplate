import { FC } from 'react'

export interface ReactLibBoilerplateProps {
  value: string
}

const ReactLibBoilerplate: FC<ReactLibBoilerplateProps> = ({ value }) => {
  return <div className="w-full">{value}</div>
}

export default ReactLibBoilerplate
