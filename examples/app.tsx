import { ReactLibBoilerplate } from '../src'

function App(): JSX.Element {
  return (
    <div className="container mx-auto flex flex-col items-center space-y-1">
      <ReactLibBoilerplate value="Hello, world!" />
    </div>
  )
}

export default App
