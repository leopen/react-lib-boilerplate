import { render } from '@testing-library/react'
import App from '../app'

describe('App', () => {
  it('should render a component', () => {
    const { queryByText } = render(<App />)
    expect(queryByText('Hello, world!')).toBeTruthy()
  })
})
